#
# Makefile to test dh-exec
#
ALLFILES = tdhe tdhe-data.sql
bindir = /usr/bin
datadir = /usr/share/tdhe

all: $(ALLFILES)

clean:
	rm -f $(ALLFILES)

tdhe:
	echo "This is tdhe" > tdhe

tdhe-data.sql:
	echo "This is tdhe-data" > tdhe-data.sql


install: $(ALLFILES)
	mkdir -p '$(DESTDIR)$(bindir)'
	install tdhe '$(DESTDIR)$(bindir)'
	mkdir -p '$(DESTDIR)$(datadir)'
	install tdhe-data.sql '$(DESTDIR)$(datadir)'



	
