README for test dh-exec
====

This is a simple Debian package to test [dh-exec](debian/dh-exec)

The key being we don't get any errors and that the data package has the pgsql file.

```
$ dpkg --contents ../test-dh-exec_1-1_amd64.deb
drwxr-xr-x root/root         0 2024-05-20 20:44 ./
drwxr-xr-x root/root         0 2024-05-20 20:44 ./usr/
drwxr-xr-x root/root         0 2024-05-20 20:44 ./usr/bin/
-rwxr-xr-x root/root        13 2024-05-20 20:44 ./usr/bin/tdhe
drwxr-xr-x root/root         0 2024-05-20 20:44 ./usr/share/
drwxr-xr-x root/root         0 2024-05-20 20:44 ./usr/share/doc/
drwxr-xr-x root/root         0 2024-05-20 20:44 ./usr/share/doc/test-dh-exec/
-rw-r--r-- root/root        20 2024-05-20 20:44 ./usr/share/doc/test-dh-exec/changelog.Debian.gz
-rw-r--r-- root/root      1647 2024-05-20 20:44 ./usr/share/doc/test-dh-exec/copyright

$ dpkg --contents ../test-dh-exec-data_1-1_all.deb
drwxr-xr-x root/root         0 2024-05-20 20:44 ./
drwxr-xr-x root/root         0 2024-05-20 20:44 ./usr/
drwxr-xr-x root/root         0 2024-05-20 20:44 ./usr/share/
drwxr-xr-x root/root         0 2024-05-20 20:44 ./usr/share/dbconfig-common/
drwxr-xr-x root/root         0 2024-05-20 20:44 ./usr/share/dbconfig-common/data/
drwxr-xr-x root/root         0 2024-05-20 20:44 ./usr/share/dbconfig-common/data/tdhe-data/
drwxr-xr-x root/root         0 2024-05-20 20:44 ./usr/share/dbconfig-common/data/tdhe-data/install/
-rwxr-xr-x root/root        18 2024-05-20 20:44 ./usr/share/dbconfig-common/data/tdhe-data/install/pgsql
drwxr-xr-x root/root         0 2024-05-20 20:44 ./usr/share/doc/
drwxr-xr-x root/root         0 2024-05-20 20:44 ./usr/share/doc/test-dh-exec-data/
-rw-r--r-- root/root        20 2024-05-20 20:44 ./usr/share/doc/test-dh-exec-data/changelog.Debian.gz
-rw-r--r-- root/root      1647 2024-05-20 20:44 ./usr/share/doc/test-dh-exec-data/copyright
```
